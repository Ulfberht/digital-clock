function updateTime() {
    var d = new Date();
    var H = ('0' + d.getHours()).slice(-2);
    var i = ('0' + d.getMinutes()).slice(-2);
    var s = ('0' + d.getSeconds()).slice(-2);
    $('.time').html(H + ':' + i + ':' + s);
    
}

var startPos;

function updateWeather() {
    if (startPos != null) {
        $.getJSON('https://query.yahooapis.com/v1/public/yql?q=select%20item.condition%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text=%22(' + startPos.coords.latitude + ',' + startPos.coords.longitude + ')%22)%20and%20u=%27c%27%20&format=json&env=store://datatables.org/alltableswithkeys', {}, function(json){  // загрузку JSON данных из файла example.json   
            $('.weather').html(setWeatherIcon(json.query.results.channel.item.condition.code) + ' ' +  ('   ' + json.query.results.channel.item.condition.temp).slice(-3) + '<i class="wi wi-celsius"></i>');
        });
    }
}

color_i = 0;
colors = [
    {'fg': '#000000', 'bg': '#878e6f'},
    //{'fg': '#000000', 'bg': '#ADADAD'},
    {'fg': '#8A0808', 'bg': '#000000'},
    //{'fg': '#ffffff', 'bg': '#0080FF'},
    {'fg': '#ffffff', 'bg': '#000000'},
    //{'fg': '#000000', 'bg': '#ffffff'},
    {'fg': '#128663', 'bg': '#000000'},
    {'fg': '#000000', 'bg': '#F76238'},
    //{'fg': '#00FFEE', 'bg': '#325050'},
];

function toggleColor() {
    color_i++;
    if (color_i >= colors.length)
        color_i = 0;
    $('body').css({'background-color': colors[color_i].bg, 'color': colors[color_i].fg});
}

function toggleWeather() {
    $('.weather').toggle();
    if ($('.weather').is(':hidden'))
        $('.weather-toggle').addClass('turnedOff');
    else
        $('.weather-toggle').removeClass('turnedOff');
    

    
}

$(function(){
updateTime();
setInterval(updateTime, 1000);
updateWeather();
setInterval(updateWeather, 5000);
$('.color-toggle').on("click", toggleColor);
$('.weather-toggle').on("click", toggleWeather);
});

window.onload = function() {
  startPos = null;
  var geoSuccess = function(position) {
    startPos = position;
  };
  var geoError = function(error) {
    var msg = 'Weather API error occurred: ';
    switch (error.code) {
        case 1: msg += 'Permission denied'; break;
        case 2: msg += 'Position unavailable (error response from location provider)'; break;
        case 3: msg += 'Timed out'; break;
        default: msg += 'Unknown error';
    }
    msg += ' (error code: ' + error.code + ')';
    console.log(msg);
    // error.code can be:
    //   0: unknown error
    //   1: permission denied
    //   2: position unavailable (error response from location provider)
    //   3: timed out
  };
  navigator.geolocation.watchPosition(geoSuccess, geoError);
};

/* source: modified https://gist.github.com/aloncarmel/8575527 */
function setWeatherIcon(condid) {
  switch(condid) {
    case '0': var icon  = '<i class="wi wi-tornado"></i>'; break;
    case '1': var icon  = '<i class="wi wi-storm-showers"></i>'; break;
    case '2': var icon  = '<i class="wi wi-tornado"></i>'; break;
    case '3': var icon  = '<i class="wi wi-thunderstorm"></i>'; break;
    case '4': var icon  = '<i class="wi wi-thunderstorm"></i>'; break;
    case '5': var icon  = '<i class="wi wi-snow"></i>'; break;
    case '6': var icon  = '<i class="wi wi-rain-mix"></i>'; break;
    case '7': var icon  = '<i class="wi wi-rain-mix"></i>'; break;
    case '8': var icon  = '<i class="wi wi-sprinkle"></i>'; break;
    case '9': var icon  = '<i class="wi wi-sprinkle"></i>'; break;
    case '10': var icon  = '<i class="wi wi-hail"></i>'; break;
    case '11': var icon  = '<i class="wi wi-showers"></i>'; break;
    case '12': var icon  = '<i class="wi wi-showers"></i>'; break;
    case '13': var icon  = '<i class="wi wi-snow"></i>'; break;
    case '14': var icon  = '<i class="wi wi-storm-showers"></i>'; break;
    case '15': var icon  = '<i class="wi wi-snow"></i>'; break;
    case '16': var icon  = '<i class="wi wi-snow"></i>'; break;
    case '17': var icon  = '<i class="wi wi-hail"></i>'; break;
    case '18': var icon  = '<i class="wi wi-hail"></i>'; break;
    case '19': var icon  = '<i class="wi wi-cloudy-gusts"></i>'; break;
    case '20': var icon  = '<i class="wi wi-fog"></i>'; break;
    case '21': var icon  = '<i class="wi wi-fog"></i>'; break;
    case '22': var icon  = '<i class="wi wi-fog"></i>'; break;
    case '23': var icon  = '<i class="wi wi-cloudy-gusts"></i>'; break;
    case '24': var icon  = '<i class="wi wi-cloudy-windy"></i>'; break;
    case '25': var icon  = '<i class="wi wi-thermometer"></i>'; break;
    case '26': var icon  = '<i class="wi wi-cloudy"></i>'; break;
    case '27': var icon  = '<i class="wi wi-night-cloudy"></i>'; break;
    case '28': var icon  = '<i class="wi wi-day-cloudy"></i>'; break;
    case '29': var icon  = '<i class="wi wi-night-cloudy"></i>'; break;
    case '30': var icon  = '<i class="wi wi-day-cloudy"></i>'; break;
    case '31': var icon  = '<i class="wi wi-night-clear"></i>'; break;
    case '32': var icon  = '<i class="wi wi-day-sunny"></i>'; break;
    case '33': var icon  = '<i class="wi wi-night-clear"></i>'; break;
    case '34': var icon  = '<i class="wi wi-day-sunny-overcast"></i>'; break;
    case '35': var icon  = '<i class="wi wi-hail"></i>'; break;
    case '36': var icon  = '<i class="wi wi-day-sunny"></i>'; break;
    case '37': var icon  = '<i class="wi wi-thunderstorm"></i>'; break;
    case '38': var icon  = '<i class="wi wi-thunderstorm"></i>'; break;
    case '39': var icon  = '<i class="wi wi-thunderstorm"></i>'; break;
    case '40': var icon  = '<i class="wi wi-storm-showers"></i>'; break;
    case '41': var icon  = '<i class="wi wi-snow"></i>'; break;
    case '42': var icon  = '<i class="wi wi-snow"></i>'; break;
    case '43': var icon  = '<i class="wi wi-snow"></i>'; break;
    case '44': var icon  = '<i class="wi wi-cloudy"></i>'; break;
    case '45': var icon  = '<i class="wi wi-lightning"></i>'; break;
    case '46': var icon  = '<i class="wi wi-snow"></i>'; break;
    case '47': var icon  = '<i class="wi wi-thunderstorm"></i>'; break;
    case '3200': var icon  =  '<i class="wi-cloud"></i>'; break;
    default: var icon  =  '<i class="wi-cloud"></i>'; break;
  }

  return icon;
}